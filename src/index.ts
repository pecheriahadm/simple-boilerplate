import './assets/styles/global.scss';

import { addHeaderColor, addClickEventListener } from './app/header';

import { renderHeader } from './app/templates/header';

const a = [1,2,3];

const init = () => {
    
    const body = document.querySelector('body');

    if (body) {
          const headers = a.map(item => renderHeader(String(item))).join(' ');
          body.innerHTML = headers;
    }
    
    const headers = document.querySelectorAll('h4');
    
    console.log(headers);

    if (headers) {
        headers.forEach(header => {
            addClickEventListener(header, () => {
                addHeaderColor(header);
            });
        })
    }
}

init();
import styles from './header.module.scss';

export const renderHeader = (text: string): string => {
   return `<h4 class="${styles.header}">${text}</h4>`
}

export const renderDescription = (text: string): string => `<div>${text}</div>`


export const card = (title: string, description: string) => { 
 
    

return `
    <div>
        ${renderHeader(title)}
        ${renderDescription(description)}
    </div>
`
}
type TStatus = 'CREATE' | 'DONE' | 'CANCELLED'

type TPriority = 'HIGH' | 'MEDIUM' | 'LOW'

export default interface ITask {
    id: number;
    title: string;
    description: string;
    createdAt: Date;
    status: TStatus;
    priority: TPriority;
}
import styles from './header.module.scss';


export const addHeaderColor = (elem: HTMLHeadingElement) => elem.classList.toggle(styles.header);


export const addClickEventListener = (elem: HTMLHeadingElement, listener: (e: MouseEvent) => void) => {
    elem.addEventListener('click', listener)
}
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/index.ts',
    output: {
      filename: 'main.js',
      path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Task manager',
            template: './src/index.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: false
            }
        }),
        new CopyWebpackPlugin([
            { from:'./src/assets/images', 
            to:'assets/images'   
        }
        ]),

    ],
    resolve: { 
        extensions: ['.js', '.ts', '.scss'], 
    },
    module: {
        rules: [
            {
                test: [/.js$|.ts$/],
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/typescript',
                        ]
                    }
                }
            },
            {
                test: /\.module\.s(a|c)ss$/,
                    loader: [
                          'style-loader',
                          {
                            loader: 'css-loader',
                            options: {
                              modules: true,
                            }
                          },
                          {
                            loader: 'sass-loader',
                          }
                        ]
            },
            {
                        test: /\.s(a|c)ss$/,
                        exclude: /\.module.(s(a|c)ss)$/,
                        loader: [
                          'style-loader',
                          'css-loader',
                          'sass-loader',
                        ]
                 },
            {
                test: /\.(png|jpg|gif|svg)$/,
                exclude: /(node_modules)/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                      name: '[name].[ext]',
                      outputPath: 'assets/images'
                    }
                  }
                ]
              }
        ]
    }
  };